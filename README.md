# 记事本


## 更新目录 
运行 ```./make_catalog.cmd``` 更新目录  
- ```快捷键ctrl+shift+b``` 
- IDE : VsCode 



# 电子书
## 游戏
- [腾讯游戏开发精粹-腾讯游戏](./电子书/游戏/腾讯游戏开发精粹-腾讯游戏.pdf)
- [unity_shaders_book_images](./电子书/游戏/unity_shaders_book_images.pdf)
- [Unity 5](./电子书/游戏/Unity 5.x Shaders and Effects Cookbook.pdf)
## 软件架构
- [software-architecture-patterns](./电子书/软件架构/software-architecture-patterns.pdf)
