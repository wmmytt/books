import os;
from io import StringIO;

def catalog_builder(dir, ioOut):

    for root, dirs, files in os.walk(dir):
        if root.find("__") != -1:
            continue
        dirname = os.path.basename(root)
        depth = root.count(os.sep) + 1
        ioOut.write("#"*depth + ' ' + dirname + '\n')
        files.sort(reverse=True)
        for file in files:
            split = file.split(".")
            f = len(split) > 0 and split[0] or file
            f = "- [" + f + '](./' + root + '/' + file + ')\n'
            f = f.replace("\\", "/")
            ioOut.write(f)

def catalog_builder_main(dir_list):
    ioOut = StringIO()
    for dir in dir_list:
        catalog_builder(dir,  ioOut)

    os.system("copy template.md README.md")
    file = open("README.md", "a", encoding="utf-8")
    file.write(ioOut.getvalue())

if __name__ == "__main__":
    catalog_builder_main(["电子书"])
    print("catalog build success!")